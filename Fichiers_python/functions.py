"""
Functions for array sort
"""
from random import randint


def gen_array(size):
    """
    Generate a random array with value between [0;2³²-1]
    With the input size
    """
    array = []
    while len(array) < size:
        array.append(randint(0, 2 ** 32 - 1))
    return array


def bubble_sort(array):
    """
    Sort an array with bubble sort method
    """
    for index_a in range(len(array), 0, -1):
        for index_b in range(0, index_a - 1):
            if array[index_b + 1] < array[index_b]:
                # switch values of array[index_b] and array[index_b + 1]
                array[index_b], array[index_b + 1] = array[index_b + 1], array[index_b]
    return array


def quick_sort(array):
    """
    Sort an array with quick sort method
    """
    if array == []:
        return []

    pivot, left, right = array[0], [], []
    for value in array[1:]:
        if value < pivot:
            left.append(value)
        else:
            right.append(value)
    return quick_sort(left) + [array[0]] + quick_sort(right)


def fusion(array_a, array_b):
    """
    Return the fusion of 2 arrays
    to perform the fusion sorting method
    """
    # If one of the two array is empty, return the other array
    if len(array_a) == 0 or len(array_b) == 0:
            if len(array_b) == 0:
                return array_a
            return array_b
    array_ab = []  # The a + b array
    index_a, index_b = 0, 0
    while len(array_ab) < len(array_a) + len(array_b):
            if array_a[index_a] < array_b[index_b]:
                array_ab.append(array_a[index_a])
                index_a += 1
            else:
                array_ab.append(array_b[index_b])
                index_b += 1
            # If index_a is at the end of an array append right part of the other array
            if index_a == len(array_a) or index_b == len(array_b):
                if array_a[index_a:] != []:
                    array_ab.extend(array_a[index_a:len(array_a)])
                elif array_b[index_b:] != []:
                    array_ab.extend(array_b[index_b:len(array_b)])
                break
    return array_ab


def fusion_sort(array):
    """
    sort an array with fusion method
    """
    if len(array) < 2:
        return array
    middle = int(len(array)/2)
    # division into 2 arrays
    array_a = fusion_sort(array[0:middle])
    array_b = fusion_sort(array[middle:len(array)])
    return fusion(array_a, array_b)
