"""
Python script, use to compare differents
methods to sort an array
"""

from timeit import default_timer as timer
import functions

# Size of arrays (Max size)
SIZE = 1
MAX_SIZE = 100000
# Execution time and the average (for the 3 methods)
EXEC_TIME = [0, 0, 0]
AVG_EXEC_TIME = [0.0, 0.0, 0.0]


# Sorting random arrays starting by the SIZE of 10
while SIZE < MAX_SIZE:
    SIZE *= 10
    # 3 folders --> 3 methods results --> 1 CSV file per size
    FILES = []
    FILES.append(open("quick_sort/tableau_taille_" + str(SIZE) + ".csv", "w"))
    FILES.append(open("fusion_sort/tableau_taille_" + str(SIZE) + ".csv", "w"))
    FILES.append(open("bubble_sort/tableau_taille_" + str(SIZE) + ".csv", "w"))

    # Head of the CSV file
    for file in FILES:
        file.write("Numero essai;")
        file.write("Temps tri (secondes);")
        file.write("Moyenne (secondes);")
        file.write("Moyenne (millisecondes)\n")
    # Iterate 100 random arrays sorting
    iteration = 1
    while(iteration <= 100):

        RANDOM_TAB = functions.gen_array(SIZE)
        # quick sort
        TAB = RANDOM_TAB
        EXEC_TIME[0] = timer()
        functions.quick_sort(TAB)
        EXEC_TIME[0] = timer() - EXEC_TIME[0]
        AVG_EXEC_TIME[0] += EXEC_TIME[0]
        # fusion sort
        TAB = RANDOM_TAB
        EXEC_TIME[1] = timer()
        functions.fusion_sort(TAB)
        EXEC_TIME[1] = timer() - EXEC_TIME[1]
        AVG_EXEC_TIME[1] += EXEC_TIME[1]
        # bubble sort 
        # (Only 10 execution for 10 000 value arrays)
        # (Only 1 execution for 100 000 value arrays)
        if not(SIZE == 10000 and iteration > 10) and not(SIZE == 100000 and iteration > 1):
            print("Debut bubbleSort, taille=" + str(SIZE) + "iteration " + str(iteration) + "/100")
            TAB = RANDOM_TAB
            EXEC_TIME[2] = timer()
            functions.bubble_sort(TAB)
            EXEC_TIME[2] = timer() - EXEC_TIME[2]
            AVG_EXEC_TIME[2] += EXEC_TIME[2]
            print("execute en " + str(EXEC_TIME[2]) + "secondes")
        # Write in files sorting times
        for index in range(0, 3):
            FILES[index].write(str(iteration) + ";")
            FILES[index].write(str(EXEC_TIME[index]) + "\n")

        iteration += 1
    # Calculate and write the averages of sorting times
    for index in range(0, 3):
        AVG_EXEC_TIME[index] = AVG_EXEC_TIME[index] / 100
        FILES[index].write(";;" + str(AVG_EXEC_TIME[index]) + ";")
        FILES[index].write(str(AVG_EXEC_TIME[index] * 1000) + "\n")

    # Close files
    for file in FILES:
        file.close()
    print("termine")
