# Nathan FOURRIER - TD8 Informatique fondamentale

# Sommaire
[Lien du projet sur git](https://gitlab.com/nathan.fourrier/td_comparaison_des_methodes_de_tri)
1. [Exercice 0](#exo0)
   
2. [Exercice 1](#exo1)
    1. [Tri à bulle](#bubblesort)
    2. [Tri par fusions](#fusionsort)
    3. [Quick sort](#quicksort)

   [Graphiques](#graphiques)

    Graphiques par tailles
      1. [Tableaux taille 10](#tab10)
      2. [Tableaux taille 100](#tab100)
      3. [Tableaux taille 1000](#tab1000)
      4. [Tableaux taille 10 000](#tab10000)
      5. [Tableaux taille 100 000](#tab100000)
   
    Graphiques par méthodes
      1. [3 méthodes](#comparaison)
      2. [Tri à bulle](#graphbbublesort)
      3. [Tri par fusion](#graphfusion)
      4.  [Quick sort](#graphquicksort)
3.  [Conclusion](#conclusion)

# Exercice 0 <a name="exo0"></a>

> Réalisez une fonction qui, à partir d’un entier n, génère un tableau d’entiers positifs de taille n.

```python
from random import randint
def gen_array(size):
    """
    Generate a random array with value between [0;2³²-1]
    With the input size
    """
    array = []
    while len(array) < size:
        array.append(randint(0, 2 ** 32 - 1))
    return array

```

# Exercice 1 <a name="exo1"></a>

## Algorithme de tri à bulles implémenté en python <a name="bubblesort"></a>

```python
def bubble_sort(array):
    """
    Sort an array with bubble sort method
    """
    for index_a in range(len(array), 0, -1):
        for index_b in range(0, index_a - 1):
            if array[index_b + 1] < array[index_b]:
                #switch values of array[index_b] and array[index_b + 1]
                array[index_b], array[index_b + 1] = array[index_b + 1], array[index_b]
    return array
```
## Algorithme de tri fusion implémenté en python <a name="fusionsort"></a>

_Nécessite 2 fonctions_

```python
def fusion(array_a, array_b):
    """
    Return the fusion of 2 arrays
    to perform the fusion sorting method
    """
    #If one of the two array is empty, return the orther array
    if len(array_a) == 0 or len(array_b)==0:
            if len(array_b) == 0:
                return array_a
            return array_b
    array_ab = [] #The a + b array
    index_a, index_b = 0, 0
    while len(array_ab) < len(array_a) + len(array_b):
            if array_a[index_a] < array_b[index_b]:
                array_ab.append(array_a[index_a])
                index_a+= 1
            else:
                array_ab.append(array_b[index_b])
                index_b+= 1
            # If index_a is at the end of an array append right part of the other array
            if index_a == len(array_a) or index_b == len(array_b):
                if array_a[index_a:] != []:
                    array_ab.extend(array_a[index_a:len(array_a)])
                elif array_b[index_b:] != []:
                    array_ab.extend(array_b[index_b:len(array_b)])
                break
    return array_ab

def fusion_sort(array):
    """
    sort an array with fusion method
    """
    if len(array) < 2:
        return array
    middle = int(len(array)/2)
    #division into 2 arrays
    array_a = fusion_sort(array[0:middle])
    array_b = fusion_sort(array[middle:len(array)])
    return fusion(array_a, array_b)
```
## Algorithme de tri quick sort implémenté en python <a name="quicksort"></a>


```python
def quick_sort(array):
    """
    Sort an array with quick sort method
    """
    if array == []:
        return []

    pivot, left, right = array[0], [], []
    for value in array[1:]:
        if value < pivot:
            left.append(value)
        else:
            right.append(value)
    return quick_sort(left) + [array[0]] + quick_sort(right)
```

# Graphiques <a name="graphiques"></a>

## Tableaux de taille 10  <a name="tab10"></a>
![Graphique pour tableaux taille 10](Images/tab_10.png)

Pour un tableau de taille 10 le tri par fusion est le plus lent.

Le temps d'éxecution du tri à bulles est inferieur mais reste tout de même proche du tri par fusion.

L'écart entre le quicksort  et les autres méthodes est déjà significatif malgré la faible taille du tableau.

## Tableaux de taille 100  <a name="tab100"></a>
![Graphique pour tableaux taille 100](Images/tab_100.png)

Dés que la taille est multipiée par 10, le bubble sort devient extrêmement plus lent que les autres méthodes.
## Tableaux de taille 1000  <a name="tab1000"></a>
![Graphique pour tableaux taille 1000](Images/tab_1000.png)

Plus la taille est grande, plus l'écart entre les méthodes se creusent.
## Tableaux de taille 10 000  <a name="tab10000"></a>
![Graphique pour tableaux taille 10 000](Images/tab_10000.png)

## Tableaux de taille 100 000  <a name="tab100000"></a>
![Graphique pour tableaux taille 100 000](Images/tab_100000.png)

Avec un tableau de 100 000 on peut clairement voir que la solution bubble sort est à bannir.

Si trier un tableau prend 32 minutes, implémenter cet algorythme pour gèrer une base de donnéee par exemple serait simplement impensable.

La méthode fusion est beaucoup plus rapide que le tri à bulles mais reste très supérieur au quicksort.


## 3 méthodes <a name="comparaison"></a>
Pour pouvoir afficher les temps d'éxecution en fonction des tailles de tableaux, il a fallu opter pour une échelle logarithmique.

![Graphique pour les 3 méthodes](Images/comparaison.png)

Sur ce premier graphique, on peut voir à quel point le bubble sort est vraiment plus lent que les autres méthodes.

## Tri à bulles <a name="graphbubblesort"></a>
 La compléxité du tri à bulle est quadratique

  Θ(n<sup>2</sup>) (idem dans le pire cas).

 L'equation de compléxité temporelle moyenne calculée avec la courbe de tendance s'en approche.
 En effet avec y=0.0003 x<sup>1.9452</sup> --> x<sup>2</sup>
 

![Graphique pour bubble sort](Images/bubblesort.png)

## Tri par fusion  <a name="graphfusion"></a>
 La compléxité d'un algortithme fuision est de forme Θ(n log(n))

 La courbe de tendance de la compléxité temporelle moyenne nous donne y = 0.0039 x<sup>1.1233</sup>

![Graphique pour fusion sort](Images/fusionsort.png)

## Quick sort <a name="graphquicksort"></a>
Pour le quick sort la compléxité dans le pire des cas est de la forme Θ(n log(n)).

La compléxité moyenne est elle de Θ(N^2).

La courbe de tendance nous donne y = 0.0017 x<sup>1.077</sup> 

x<sup>1.077</sup> s'approche de x<sup>1</sup> ce qui donne --> 0.0017 

![Graphique pour quick sort](Images/quicksort.png)

# Conclusion <a name="conclusion"></a>

## Comparaisons tri

Nous pouvons remarquer à la suite de cette analyse que la méthode de **tri à bulles** qui est **simple à implémenter** est cependant **complexe à exectuer** et **peu performante**.

C'est pour cela qu'il faut prioriser la méthode **quicksort**, malgré son **implémentation difficile** à mettre en place, elle permet de très bonne **performances**.

## Note
Cette analyse montre également que mesurer le temps d'éxecution d'un programme permet de donner une idéee du comportement de celui-ci.

**Cependant cette méthode ne vaut pas une réelle étude de compléxité qui sera fiable.**